package main

import(
	"fmt"
	"log"
	"time"
	"context"
	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// creating instance of a session with mongodb which will interact with the database
type MongoInstance struct {
	Client *mongo.Client
	Db     *mongo.Database
}

// creating variable to access the isntane
var mg MongoInstance

const dbName = "fiber-hrms"
// to connect the fiber-hrms database inside mongoDB
const mongoURI = "mongodb://localhost:27017" + dbName


type Employee struct {
	// defining the JSON, BSON representation of the datatypes
	// for golang(ID), for JSON(id), for mongodb(_id)
	ID      string   `json:"id, omitempty" bson:"_id, omitempty"`
	Name    string   `json:"name"`
	Salary  float64  `json:"salary"`
	Age     float64  `json:"age"`
}

// to connect golang to mongodb
func Connect() error{
	// using the URI create and connect to a new client
	client, err := mongo.NewClient(options.Client().ApplyURI(mongoURI))
	// using the context which mongodb have to set a timeout so in case if mongodb doesn't respond or taking more time then it will not stop the whole program
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	// to make "ctrl+c to cancel everything" feature enabled
	defer cancel()

	err = client.Connect(ctx)
	db := client.Database(dbName)

	if err != nil {
		return err
	}

	// setting the client and db value of the mg instance
	mg = MongoInstance {
		Client: client,
		Db:     db,
	}
	return nil

}

func main() {
	if err := Connect(); err != nil {
		log.Fatal(err)
	}
	// to hold the fiber instance
	app := fiber.New()

	// standard functions or routes
	// when get request is sent to emplyee route, it returns the function
	app.Get("/employee", func(c *fiber.Ctx) error {

	})
	app.Post("/employee")
	app.Put("/employee/:id")
	app.Delete("/employee/:id")
}